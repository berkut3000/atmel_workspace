/*
 * clock_setup.c
 *
 * Created: 04/09/2017 09:33:36 a.m.
 *  Author: hydro
 */ 
#include "clock_setup.h"

void xtal_config()
{
	//enable external crystal
	REG_CKGR_MOR |= CKGR_MOR_KEY_PASSWD | CKGR_MOR_MOSCXTEN;
	//wait for crystal to become ready
	while (!(REG_PMC_SR & PMC_SR_MOSCXTS));
	//select crystal
	REG_CKGR_MOR |= CKGR_MOR_KEY_PASSWD | CKGR_MOR_MOSCSEL;
	//master clock source selection - choose main clock
	REG_PMC_MCKR |= PMC_MCKR_CSS_MAIN_CLK;
	
}

void pll_config()
{
	//enable external crystal
	REG_CKGR_MOR |= CKGR_MOR_KEY_PASSWD | CKGR_MOR_MOSCXTEN;
	//wait for crystal to become ready
	while (!(REG_PMC_SR & PMC_SR_MOSCXTS));
	//select crystal
	REG_CKGR_MOR |= CKGR_MOR_KEY_PASSWD | CKGR_MOR_MOSCSEL;
	
	////8MHz / 4 = 2MHz, 2MHz * (9+1) = 20MHz
	//REG_CKGR_PLLBR |= CKGR_PLLBR_MULB(1);
	//REG_CKGR_PLLBR |= CKGR_PLLBR_DIVB(1);
	
	//8MHz / 2 = 4MHz, 4MHz * (24+1) = 20MHz
	REG_CKGR_PLLBR |= CKGR_PLLBR_MULB(10);
	REG_CKGR_PLLBR |= CKGR_PLLBR_DIVB(2);

	//select PLLB as the master clock
	//master clock source selection - choose main clock
	REG_PMC_MCKR |= PMC_MCKR_CSS_PLLB_CLK;
	
}
/*void core1_setup()
{
	
	
}*/

void clock_init(){
	//xtal_config();
	pll_config();
	
	
	//wait until main clock ready
	while (!(REG_PMC_SR & PMC_SR_MCKRDY));
	//select processer prescaler (0 - no divisor)
	REG_PMC_MCKR |= PMC_MCKR_PRES_CLK_1;
	//wait until main clock ready
	while (!(REG_PMC_SR & PMC_SR_MCKRDY));
	//core1_setup();
}