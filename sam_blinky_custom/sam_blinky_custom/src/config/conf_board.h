/**
 * \file
 *
 * \brief User board configuration template
 *
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */

#ifndef CONF_BOARD_H
#define CONF_BOARD_H

//Led
#define LED0    IOPORT_CREATE_PIN(PIOC, 6)

#endif // CONF_BOARD_H
