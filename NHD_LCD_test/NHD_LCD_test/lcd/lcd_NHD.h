/*
 * lcd_NHD.h
 *
 * Created: 22/01/2018 07:04:40 p.m.
 *  Author: hydro
 */ 


#ifndef LCD_NHD_H_
#define LCD_NHD_H_

#include "sam4c.h"
//#include "pio/sam4c16c.h"
#include "../delay/delay_conf.h"
//#include "imagenes.h"

#define	dot		11
#define	ddot	10
#define lA		0
#define lB		1
#define lC		2
#define lD		3
#define lE		4
#define lF		5
#define	lG		6
#define lH		7
#define	lI		8
#define	lJ		9
#define	lK		10
#define	lL		11
#define	lM		12
#define	lN		13
#define	lO		14
#define	lP		15
#define	lQ		16
#define	lR		17
#define	lS		18
#define	lT		19
#define	lU		20
#define lV		21
#define	lW		22
#define	lX		23
#define	lY		24
#define	lZ		25

//#include "C:\Program Files (x86)\Atmel\Studio\7.0\packs\atmel\SAM4C_DFP\1.0.86\sam4c\include\pio\sam4c16c.h"

//Definciones de los pines que controlan a la pantalla LCD NHD-12832A01z
//CS
#define LCD_CS_ID		(ID_PIOA)
#define LCD_CS_PIO		(PIOA)
#define LCD_CS_PORT		(IOPORT_PIOA)
#define LCD_CS_MASK		(PIO_PA22)
//RST
#define LCD_RST_ID		(ID_PIOB)
#define LCD_RST_PIO		(PIOB)
#define LCD_RST_PORT	(IOPORT_PIOB)
#define LCD_RST_MASK	(PIO_PB12)
//Instr/Data
#define LCD_A0_ID		(ID_PIOA)
#define LCD_A0_PIO		(PIOA)
#define LCD_A0_PORT		(IOPORT_PIOA)
#define LCD_A0_MASK		(PIO_PA0)
//SCK
#define LCD_SCK_ID		(ID_PIOA)
#define LCD_SCK_PIO		(PIOA)
#define LCD_SCK_PORT	(IOPORT_PIOA)
#define LCD_SCK_MASK	(PIO_PA8)
//MOSI
#define LCD_MOSI_ID		(ID_PIOA)
#define LCD_MOSI_PIO	(PIOA)
#define LCD_MOSI_PORT	(IOPORT_PIOA)
#define LCD_MOSI_MASK	(PIO_PA7)
//Habilitar/deshabilitar los pines-----------------------------
//CS
#define LCD_CS_PIO_En()		(LCD_CS_PIO->PIO_PER |= LCD_CS_MASK)
#define LCD_CS_PIO_Dis()	(LCD_CS_PIO->PIO_PDR |= LCD_CS_MASK)
//RST
#define LCD_RST_PIO_En()	(LCD_RST_PIO->PIO_PER |= LCD_RST_MASK)
#define LCD_RST_PIO_Dis()	(LCD_RST_PIO->PIO_PDR |= LCD_RST_MASK)
//SCK
#define LCD_SCK_PIO_En()	(LCD_SCK_PIO->PIO_PER |= LCD_SCK_MASK)
#define LCD_SCK_PIO_Dis()	(LCD_SCK_PIO->PIO_PDR |= LCD_SCK_MASK)
//Instr/Data
#define LCD_A0_PIO_En()		(LCD_A0_PIO->PIO_PER |= LCD_A0_MASK)
#define LCD_A0_PIO_Dis()	(LCD_A0_PIO->PIO_PDR |= LCD_A0_MASK)
//MOSI
#define LCD_MOSI_PIO_En()	(LCD_MOSI_PIO->PIO_PER |= LCD_MOSI_MASK)
#define LCD_MOSI_PIO_Dis()	(LCD_MOSI_PIO->PIO_PDR |= LCD_MOSI_MASK)
//Establecer como entrada/salida-----------------------------------
//CS
#define LCD_CS_Out()		(PIOA->PIO_OER |= LCD_CS_MASK)
#define LCD_CS_In()			(PIOA->PIO_ODR |= LCD_CS_MASK)
//RST
#define LCD_RST_Out()		(PIOB->PIO_OER |= LCD_RST_MASK)
#define LCD_RST_In()		(PIOB->PIO_ODR |= LCD_RST_MASK)
//SCK
#define LCD_SCK_Out()		(PIOA->PIO_OER |= LCD_SCK_MASK)
#define LCD_SCK_In()		(PIOA->PIO_ODR |= LCD_SCK_MASK)
//Instr/Data
#define LCD_A0_Out()		(PIOA->PIO_OER |= LCD_A0_MASK)
#define LCD_A0_In()			(PIOA->PIO_ODR |= LCD_A0_MASK)
//MOSI
#define LCD_MOSI_Out()		(PIOA->PIO_OER |= LCD_MOSI_MASK)
#define LCD_MOSI_In()		(PIOA->PIO_ODR |= LCD_MOSI_MASK)
//Habilitar resistencias de Pull-Up/Down
//CS
#define LCD_CS_Pullup_En()	(PIOA->PIO_PUER |= LCD_CS_MASK)
#define LCD_CS_Pullup_Dis()	(PIOA->PIO_PUDR |= LCD_CS_MASK)
//RST
#define LCD_RST_Pullup_En() (PIOB->PIO_PUER |= LCD_RST_MASK)
#define LCD_RST_Pullup_Dis()(PIOB->PIO_PUDR |= LCD_RST_MASK)
//SCK
#define LCD_SCK_PUllup_En()	(PIOA->PIO_PUER |= LCD_SCK_MASK)
#define LCD_SCK_Pullup_Dis()(PIOA->PIO_PUDR |= LCD_SCK_MASK)
//Instr/Data
#define LCD_A0_Pullup_En()	(PIOA->PIO_PUER |= LCD_A0_MASK)
#define LCD_A0_Pullup_Dis() (PIOA->PIO_PUDR |= LCD_A0_MASK)
//MOSI
#define LCD_MOSI_Pullup_En() (PIOA->PIO_PUER |= LCD_MOSI_MASK)
#define LCD_MOSI_Pullup_Dis()(PIOA->PIO_PUDR |= LCD_MOSI_MASK)

//Accion de alto/bajo
#define LCD_CS_High()		(PIOA->PIO_SODR	|= LCD_CS_MASK)
#define LCD_CS_Low()		(PIOA->PIO_CODR	|= LCD_CS_MASK)
//RST
#define LCD_RST_High()		(PIOB->PIO_SODR |= LCD_RST_MASK)
#define LCD_RST_Low()		(PIOB->PIO_CODR |= LCD_RST_MASK)
//SCK
#define LCD_SCK_High()		(PIOA->PIO_SODR |= LCD_SCK_MASK)
#define LCD_SCK_Low()		(PIOA->PIO_CODR |= LCD_SCK_MASK)
//Instr/Data
#define LCD_A0_High()		(PIOA->PIO_SODR |= LCD_A0_MASK)
#define LCD_A0_Low()		(PIOA->PIO_CODR |= LCD_A0_MASK)
//MOSI
#define LCD_MOSI_High()		(PIOA->PIO_SODR |= LCD_MOSI_MASK)
#define LCD_MOSI_Low()		(PIOA->PIO_CODR |= LCD_MOSI_MASK)


//Funciones del LCD
//Emitir dato
void data_out(unsigned char i);
//Emitir comando
void comm_out(unsigned char i);
//Iniciar LCD
void init_LCD_NHD();
//mostrarImagen
void mosImg(unsigned char *cadena_lcd);
//mostrar renglon
void mosImgRenglon(unsigned char *cadena_lcd, unsigned char page, unsigned char startPos);
//ejecutar demo
void cuadro_test_mosImg(unsigned char *cadena_lcd);
//encender display
void encender_display(unsigned char i);
//ajustar contraste
void ajustar_contraste(unsigned char i);
//todos los pixeles negros
void blackout(unsigned char i);
//reseteo de la pantalla
void rst_lcd();
//fondo blanco, pixeles negros = 0; fondo negro, pixeles blancos = 1
void fn_pb(unsigned char i);
//Direccion de escaneo; normal = 0; invertido = 1
void inv_scan(unsigned char i);
//limpieza de la pantalla
void lcd_clear(unsigned int l);

//mandar una sola columna
void mosColumna(unsigned char *cadena_lcd, unsigned char page, unsigned char startPos);
//mandar una sola letra
void mosLetra(unsigned char *cadena_lcd, unsigned char page, unsigned char startPos, unsigned char posArray, unsigned char cols);
//mostrar letrero de cidesi
void mosCidesi();
//mostrar Voltaje
void mosVol(unsigned char *array);
//mostrar Corriente
void mosCur(unsigned char *array);
//mostrar Potencia
void mosWat(unsigned char *array);
//mostrar Consumo
void mosCon(unsigned char *array);
//borrar renglon a partir de cierta posicion
void renglon_clear(unsigned char renglon, unsigned char pos);

#endif /* LCD_NHD_H_ */