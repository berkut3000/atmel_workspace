/**
 * \file
 *
 * \brief Empty user application template
 *
 */

/**
 * \mainpage User Application template doxygen documentation
 *
 * \par Empty user application template
 *
 * Bare minimum empty user application template
 *
 * \par Content
 *
 * -# Include the ASF header files (through asf.h)
 * -# "Insert system clock initialization code here" comment
 * -# Minimal main function that starts with a call to board_init()
 * -# "Insert application code here" comment
 *
 */

/*
 * Include header files for all drivers that have been imported from
 * Atmel Software Framework (ASF).
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */
#include <asf.h>

int main (void)
{
	/* Insert system clock initialization code here (sysclk_init()). */
	sysclk_init();
	board_init();
	WDT->WDT_MR = WDT_MR_WDDIS;

	REG_PIOC_PER = PIO_PER_P6;         // enable PIO to control pin PB14
	REG_PIOC_OER = PIO_OER_P6;         // enable PB14 as an output pin
	
	while (1) {
		REG_PIOC_CODR = PIO_CODR_P6;   // switch LED on
		delay_ms(500);
		REG_PIOC_SODR = PIO_SODR_P6;   // switch LED off
		delay_ms(500);
	}
}
