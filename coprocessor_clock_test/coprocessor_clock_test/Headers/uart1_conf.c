/*
 * uart1_conf.c
 *
 * Created: 18/09/2017 06:56:02 p.m.
 *  Author: hydro
 */ 

#include "uart1_conf.h"

void uart1_config()
{
	//---UART1 CFG
	
	REG_PIOC_PDR |= PIO_PDR_P0;
	//disable PIOA control of PC0 and enable peripheral on pin
	REG_PIOC_PDR |= PIO_PDR_P1;//the same is done for PC1
	
	REG_PIOC_ABCDSR &=  ~(PIO_ABCDSR_P0);
	REG_PIOC_ABCDSR &=  ~(PIO_ABCDSR_P1);
	
	
	//configure PMC UART Clock
	//enable UART1 clock
	REG_PMC_PCER1 |= PMC_PCER1_PID38;
	
	//configure buad rate
	REG_UART1_BRGR |= 17;
	//fcpu/16xBR 16,000,000/(16x57600)
	//It seems that 57600 is the maximum baudrate permitted by the coprocessor.
	//Because the main one allows seamlessly to communicate at 115200 with a >= 16Mhz clock.
	
	//parity
	REG_UART1_MR |= UART_MR_PAR_NO;
	
	//mode
	//normal mode default
	
	//enable transmit/receive
	REG_UART1_CR |= UART_CR_TXEN;
	REG_UART1_CR |= UART_CR_RXEN;
	
	//enable interrupt on receive
	REG_UART1_IER |= UART_IER_RXRDY;
	
	//UART1 CFG ends here
}

void transmitByte(uint8_t data){
	//wait for ready
	while (!(REG_UART1_SR & UART_SR_TXRDY));
	while (!(REG_UART1_SR & UART_SR_TXEMPTY));
	REG_UART1_THR |= data;
}

void printString(const char myString[]) {
	uint8_t i = 0;
	while (myString[i]) {
		transmitByte(myString[i]);
		i++;
	}
}

void printNewLine()
{
	transmitByte(13);
	transmitByte(10);
}

void UART1_Handler(void) {
    // when we receive a byte, transmit that byte back
    uint32_t status = REG_UART1_THR;
    if ((status & UART_SR_RXRDY)){
        //read receive holding register
        uint8_t readByte = REG_UART1_THR;
        //transmit that byte back
        transmitByte(readByte);
    }
}