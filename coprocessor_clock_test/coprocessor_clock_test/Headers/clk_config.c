/*
 * clk_config.c
 *
 * Created: 18/09/2017 06:49:35 p.m.
 *  Author: hydro
 */ 
#include "clk_config.h"

void cpclk_conf_init()
{
	//wait until main clock ready
	while (!(REG_PMC_SR & PMC_SR_MCKRDY));
	//select processer prescaler (0 - no divisor)
	REG_PMC_MCKR |= PMC_MCKR_PRES_CLK_1;
	//wait until main clock ready
	while (!(REG_PMC_SR & PMC_SR_MCKRDY));
	
	
	unsigned int read_reg = 0;//variable for storing clock configurations.
	
	//---reset core1 and core1 peripheral------------------
	REG_RSTC_CPMR = RSTC_CPMR_CPKEY_PASSWD;
	
	//Enables Coprocessor Bus Master Clock
	REG_PMC_SCER = PMC_SCER_CPBMCK | PMC_SCER_CPKEY_PASSWD;
	
	//Enables Coprocessor Clocks
	REG_PMC_SCER = PMC_SCER_CPCK | PMC_SCER_CPKEY_PASSWD;
	
	//Set Coprocessor prescaler and source
	read_reg = REG_PMC_MCKR;
	read_reg &= ~PMC_MCKR_CPPRES_Msk;
	
	// 102.4MHz for poly-phase AFE
	read_reg |= PMC_MCKR_CPPRES( 1 - 1 );	//CONFIG_CPCLK_PRES =0;
	
	REG_PMC_MCKR = read_reg;
	
	//CHoose Coprocessor main clock source
	read_reg = REG_PMC_MCKR;
	read_reg &= ~PMC_MCKR_CPCSS_Msk;
	read_reg |= ( 4 << PMC_MCKR_CPCSS_Pos );//4 means using main clock
	REG_PMC_MCKR = read_reg;
	
	//Release coprocessor peripheral reset
	REG_RSTC_CPMR = (RSTC_CPMR_CPKEY( 0x5Au) | RSTC_CPMR_CPEREN);
	
	// Enable Core 1 SRAM1 and SRAM2 memories //
	REG_PMC_PCER1 |= PMC_PCER1_PID42;
	
	
	//deasserts Coprocessor reset
	//REG_RSTC_CPMR = RSTC_CPMR_CPEREN | RSTC_CPMR_CPROCEN | RSTC_CPMR_KEY_PASSWD;
	REG_RSTC_CPMR = RSTC_CPMR_CPEREN | RSTC_CPMR_CPROCEN | RSTC_CPMR_CPKEY_PASSWD;
}

void clock_init()
{
	//enable external crystal
	REG_CKGR_MOR |= CKGR_MOR_KEY_PASSWD | CKGR_MOR_MOSCXTEN;
	//wait for crystal to become ready
	while (!(REG_PMC_SR & PMC_SR_MOSCXTS));
	//select crystal
	REG_CKGR_MOR |= CKGR_MOR_KEY_PASSWD | CKGR_MOR_MOSCSEL;
	
	//As the maximum clock is 8MHz, the PLLB clock must be used and set to 16Mhz
	//8MHz/1 = 8Mhz, 8MHz * (1+1) = 16 Mhz
	REG_CKGR_PLLBR |= CKGR_PLLBR_MULB(1);//Multiplier of 1
	REG_CKGR_PLLBR |= CKGR_PLLBR_DIVB(1);//divider of 1
	
	//select PLLB as the master clock
	REG_PMC_MCKR |= PMC_MCKR_CSS_PLLB_CLK;
	
	
	/* the following is just in the given that the use of 8Mhz clock is desired. The previous selection must be commented in that case
	////master clock source selection - choose main clock
	//REG_PMC_MCKR |= PMC_MCKR_CSS_MAIN_CLK;
	*/
	
	//Coprocessor clock configuration
	cpclk_conf_init();	
}