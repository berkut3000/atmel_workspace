/*
 * coprocessor_clock_test.c
 *
 * Created: 05/09/2017 03:41:41 p.m.
 * Author : hydro
 * Based on examples written by Robert McMillan
 */ 
#include "sam.h" //Include Sam library for dependencies
#include "../Headers/clk_config.h"
#include "../Headers/uart1_conf.h"
#include "../Headers/delay_func.h"

int main(void)
{
	/* Initialize the SAM system */
	SystemInit();
	clock_init();
	uart1_config();
	REG_WDT_MR = WDT_MR_WDDIS;//disable watchdog timer, this is important in this case because if just one string
	//were to be sent, the program would end right there depite the loop. So the WDog timer would recognize this
	//as an error and reset the whole chip.
	NVIC_EnableIRQ(UART1_IRQn);
	
	printString("Waiting for input\r\n");
	//NVIC_EnableIRQ(UART1_IRQn);//Enable interrupt for UArt trasnmit
	
	char cad[] = "\"Voltage\"\:127.34 \"Current\"\:17.22 \"PF\"\:23.12 \"Consumption\":231.34 ";
	char newLine[] = "\r\n";
	char i;
		
	 while (1)
	 {
		 //continously send a string
		 printString(cad);
		 printNewLine();
		 for (i= 50; i>0; i--)
		 {
			 delay();			 
		 }
		 
		 
	 }
}

